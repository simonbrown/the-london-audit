var service = new google.maps.DistanceMatrixService();

// Source: https://en.wikipedia.org/wiki/List_of_busiest_London_Underground_stations
var stations = [
   "Waterloo",
   "King's Cross St. Pancras",
   "Victoria",
   "Oxford Circus",
   "Liverpool Street",
   "London Bridge",
   "Bank & Monument",
   "Paddington",
   "Euston",
   "Piccadilly Circus",
   "Green Park",
   "Bond Street",
   "Tottenham Court Road",
   "Leicester Square",
   "Holborn",
   "South Kensington",
];

var destinations = stations.map(x => x + " underground station");

function get_travel_times(query)
{
  return new Promise(function(resolve, reject)
  {
    var depart_time = new Date();
    // Set departure time as 7pm so the results are consistent throughout the day.
    depart_time.setHours(19);
   
    service.getDistanceMatrix({
       origins: [query],
       destinations: destinations,
       travelMode: "TRANSIT",
       transitOptions: {
          departureTime: depart_time
       }
    }, function(response, status)
    {
      if (status === "OK")
      {
        var origin_interpretation = response["originAddresses"][0];
   
        var destination_info = response["rows"][0]["elements"];
        
        if (destination_info[0].status === "OK")
        {
          var destination_times = destination_info.map(x => x.duration.value);
          var average_time = destination_times.reduce((x, y) => x + y, 0) / destination_times.length;
          var average_time_minutes = Math.round(average_time / 60);
   
          destination_info = destination_info.map(function(x, i) {
             x.title = stations[i];
             return x;
          });
          
          resolve({
            "average": average_time_minutes,
            "items": destination_info,
            "origin": origin_interpretation
          });
        } else {
          reject("Route not found: "+ destination_info[0].status);
        }
      } else { // status !== OK
        reject("Query failed: " + status);
      }
    });
  });
}

function resolved(data)
{
  var source = document.getElementById("template").innerHTML;
  var template = Handlebars.compile(source);
  
  var html = template(data);

  document.getElementById("main_box").innerHTML = html;
}

function rejected(error)
{
  var source = document.getElementById("template_error").innerHTML;
  var template = Handlebars.compile(source);
  
  console.log(error);

  var html = template({
    "error_message": "Could not find route"
  });

  document.getElementById("main_box").innerHTML = html;
}

document.getElementById("search_form").addEventListener("submit", function(ev) {
   ev.preventDefault();
   
   var query = document.getElementById("query").value;
   
   document.getElementById("main_box").innerHTML = "";
   
   get_travel_times(query).then(resolved, rejected);
});